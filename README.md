# Plugin usage tracker

A WordPress library containing interfaces, abstracts and implementations to be used for plugins' usage tracking.

[[TOC]]

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer require wpdesk/wp-wpdesk-tracker
```

## Usage

The library features multiple purposes:
- provides a framework for collecting website usage data,
- prepares a common set of data to be sent to the tracker server,
- exposes UI elements for user to interact with the tracker (enable/disable telemetry)

### Extending collected data

If you want to include custom data exposed by your plugin, you can easily add it to the usage data payload. There are two ways to register additional data:

- via `wpdesk_tracker_data` filter,
- by registering class implementing `WPDesk_Tracker_Data_Provider` interface

### Exposing to users

As usage tracking is disabled by default, a key point is to encourage users to enable it. This library exposes some common UI interfaces, which serves the purpose of enabling the telemetry:

- admin notice, encouraging user to enable usage tracking,
- a box with information about tracking and actionable buttons

```php
/**
 * Assuming usage with wp-builder library.
 */

use WPDesk\Tracker\OptInOptOut;
use WPDesk\Tracker\OptInPage;
use WPDesk\PluginBuilder\Plugin\AbstractPlugin;

class Plugin extends AbstractPlugin {
  public function hooks(): void {
	$tracker_ui = new OptInOptOut(
		$this->plugin_info->get_plugin_file_name(),
		$this->plugin_info->get_plugin_slug(),
		'https://wpdesk.net',
		$this->plugin_info->get_plugin_name()
	);
	$tracker_ui->create_objects();
	$tracker_ui->hooks();
  }

  public function settings_page(): void {
    echo "<h1>Settings page</h1>";

	(new OptInPage(
		$this->plugin_info->get_plugin_file_name(),
		$this->plugin_info->get_plugin_slug()
	))->output();
  }
}
```

### Replacing the default sender

By default, the library sends usage data to wpdesk.org server. You can replace it with your desired endpoint, by swapping `WPDesk_Tracker_Sender` implementation.

```php
class MySender implements WPDesk_Tracker_Sender {
	public function send_payload(array $payload){
	   // implement send_payload method.
	}
}

$plugin_slug = 'my-example-plugin';
add_filter( 'wpdesk/tracker/sender/' . $plugin_slug, 'replace_sender' );

function replace_sender() {
	return new MySender();
}
```

## Technical details

The library enables usage tracking globally for all library consumers. This means, if you have two separate plugins using the library, the data will be send only once.

After enabling, data collection is performed periodically, once a week.
